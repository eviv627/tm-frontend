import React from "react";

import Header from "./Header/Header";
import Page from "./Page";

import "./App.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: true,
      page: "Projects"
    };
    this.loginHandle = this.loginHandle.bind(this);
    this.pageHandle = this.pageHandle.bind(this);
  }

  loginHandle() {}

  pageHandle() {}

  render() {
    return (
      <div>
        <Header isLoggedIn={this.isLoggedIn} pageHandle={this.pageHandle} />
        <Page page={this.state.page} loginHandle={this.loginHandle} />
      </div>
    );
  }
}

export default App;
