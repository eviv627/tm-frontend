import React from "react";

import Login from "./Login";
import Projects from "./Projects";
import Tasks from "./Tasks";

class Page extends React.Component {
  constructor(props) {
    super();
    this.state = {
      page: props.page,
      loginHandle: props.loginHandle
    };
  }

  render() {
    if (this.state.page === "Projects") {
      return <Projects />;
    }

    if (this.state.page === "Tasks") {
      return <Tasks />;
    }
    console.log(this.state.page);
    return <Login />;
  }
}

export default Page;
