import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

class Projects extends React.Component {
  constructor(props) {
    super();
    this.state = {
      columns: [
        {
          dataField: "id",
          text: "Project ID"
        },
        {
          dataField: "name",
          text: "Project Name"
        },
        {
          dataField: "description",
          text: "Project Description"
        }
      ],

      projects: [
        {
          id: 1,
          name: "one",
          description: "one"
        }
      ]
    };
  }

  render() {
    return (
      <BootstrapTable
        keyField="id"
        data={this.state.projects}
        columns={this.state.columns}
      />
    );
  }
}

export default Projects;
